const morpion = document.querySelector(".morpion");
const btnStart = document.querySelector(".btn-start");
const maxCase = 9;
let firstPlayer = true;
let nbreCaseRempli = 0;


morpion.addEventListener("click", clickCase);
btnStart.addEventListener("click", clearCase);

function clickCase(e) {
  const maCase = e.target;
  if (maCase.classList.contains("col")) {
    if (maCase.innerText == "") {
      maCase.innerText = firstPlayer ? "X" : "O";
      testWin(maCase.innerText);
      firstPlayer = !firstPlayer;
      nbreCaseRempli++;
      if (nbreCaseRempli == maxCase) {
        btnStart.style.display = "block";
      }
    }
  }
}

function clearCase(e) {
  const cases = document.querySelectorAll(".morpion .col");
  for (let c of cases) {
    c.innerText = "";
  }
  e.target.style.display = "none";
  nbreCaseRempli = 0;
}

function testWin(joueur) {
  testWinRowCol(joueur, "i");
  testWinRowCol(joueur, "j");
  testWinDiag(joueur, "d");
  testWinDiag(joueur, "r");
}

function testWinRowCol(joueur, ax) {
  for (let i = 1; i <= 3; i++) {
    const cases = document.querySelectorAll(
      ".morpion .col[data-" + ax + "='" + i + "']"
    );
    testCases(cases, joueur);
  }
}

function testWinDiag(joueur, diag) {
  const casesDiag = document.querySelectorAll(
    ".morpion .col[data-" + diag + "='1']"
  );
  testCases(casesDiag, joueur);
}

function testCases(cases, joueur) {
  let win = true;
  for (let c of cases) {
    if (c.innerText != joueur) {
      win = false;
      break;
    }
  }
  if (win) {
    alert("Bravo le joueur " + joueur + " a gagné");
    btnStart.style.display = "block";
  }
}
